const dictionary = ['javascript', 'java', 'python']

/*
functia sanitize primeste ca parametru un text si un dictionar
cuvintele din dictionar sunt inlocuite in text cu prima litera urmata de o serie de asteriscuri (egala cu numarul de litere inlocuite) urmata de ultima litera
e.g. daca 'decembrie' exista in dictionar, va fi inlocuit cu 'd*****e'
*/
function sanitize(text, dictionary){
    for (let i=0;i<dictionary.length;i++)
    {
        let word = dictionary[i];
        for(let j=1;j<word.length-1;j++){
            word=word.replace(word[j], '*')
        }
        text=text.replace(dictionary[i], word);
    }
    return text;
    
}

module.exports.dictionary = dictionary
module.exports.sanitize = sanitize