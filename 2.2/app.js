class Person{
	constructor(name){
		this.name = name
	}
}

class Student extends Person{
	constructor(name, faculty, year){
		super(name)
		this.faculty = faculty
		this.year = year
		this.grades = []
	}
	
	addGrade(grade){
		this.grades.push(grade)
	}
	
	isPassing(){
		let j = this.grades.map(x => x.value)
		let f=1
		for(let i=0;i<j.length;i++){
			if(j[i]<5)
			{
				f=0
				return false
			}
		}
		if(f==1)
		return true
	}
	
	getPassingGrades(){
		let z= this.grades.filter( x => x.value >=5)
		let w= z.map( q => q.value)
		return w
	}
	getFailedSubjects(){
		let w = this.grades.filter(x => x.value <5)
		let q=w.map( x => x.subject)
		return q
	}
}

class Grade{
	constructor(subject, value){
		this.subject = subject
		this.value = value
	}
	
	isPassingGrade(){
		return this.value >=5
	}
}

module.exports.Student = Student
module.exports.Grade = Grade